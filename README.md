# Agoda

Assumption :
  - at least java 8 is installed
	* if not download and install https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
	* Set up environment variable JAVA_HOME
  - Maven is installed
	* download and extract apache-maven-3.6.1-bin in http://maven.apache.org/download.cgi
	* add %MAVEN_HOME%\bin\ into environment variable PATH
	* to check if mvn is setup run command in console "mvn --version"
  
Execution :
  - go to maven project directory (with pom.xml) ex. C:\excercise\Agoda
  - mvn surefire-report:report
  - test report is found at %projcect_home%\target\surefire-reports
  
Build will fail
  - intentionally created one scenario that will fail.
  
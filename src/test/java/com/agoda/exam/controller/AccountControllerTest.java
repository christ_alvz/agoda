package com.agoda.exam.controller;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.Contains;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.agoda.exam.AgodaApplication;
import com.agoda.exam.bean.Account;
import com.agoda.exam.repository.AccountRepository;
import com.agoda.exam.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AgodaApplication.class)
public class AccountControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;
	private MockMvc mvc;

	@Before
	public void setUp() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void create_invalid_password_only_small_caps() throws Exception {
		// invalid_password_only_small_caps
		Account account = new Account("SampleName", "SampleUserName", "", "qwertyuioplkjhgfds");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable())
				.andReturn().getResolvedException().getMessage();
		System.out.println(errorMessage + "++++++");
		assertTrue(errorMessage.contains("At least 1 Upper case, 1 lower case ,least 1 numeric, 1 special character"));
	}
	
	@Test
	public void create_invalid_password_no_special_Character() throws Exception{
		Account account = new Account("name", "username", "", "A1thequickbrownfoxjumpsverthedg");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		assertTrue(errorMessage.contains("At least 1 Upper case, 1 lower case ,least 1 numeric, 1 special character"));
	}
	
	@Test
	public void create_invalid_password_no_uppercase() throws Exception{
		// invalid password no uppercase
		Account account = new Account("name", "username", "", "1thequickbrownfoxjumpsverthedg");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
					.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
			assertTrue(errorMessage.contains("At least 1 Upper case, 1 lower case ,least 1 numeric, 1 special character"));
	}
	@Test
	public void create_less_than_18_characters() throws Exception{

		// less than 18 characters
		Account account = new Account("name", "username", "", "1@Pumpsverthedg");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		System.out.println(errorMessage + "++++++");
		assertTrue(errorMessage.contains("Password is too short"));
	}
	@Test
	public void create_all_number() throws Exception{
	// all number
		Account account = new Account("name", "username", "", "123456789012345678");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		assertTrue(errorMessage.contains("At least 1 Upper case, 1 lower case ,least 1 numeric, 1 special character"));
	}
	
	@Test
	public void create_all_special_characters() throws Exception{
		// all special Characters
		Account account = new Account("name", "username", "", "@@@@@@@@@@@@@@@@@@@@@@@@");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		assertTrue(errorMessage.contains("At least 1 Upper case, 1 lower case ,least 1 numeric, 1 special character"));
	}
	
	@Test 
	public void create_with_invalid_special_character() throws Exception{
		// With invalid Special Character
		Account account = new Account("name", "username", "", "^()&^%$#@!(*&^%$#@!($%$*&^%%$");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		
		assertTrue(errorMessage.contains("At least 1 Upper case, 1 lower case ,least 1 numeric, 1 special character"));
	}
	@Test 
	public void create_empty_password() throws Exception{
		// empty password
		Account account = new Account("name", "username", "", "");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		assertTrue(errorMessage.contains("Password is too short"));
	}
	@Test
	public void create_more_than_50_percent_digit() throws Exception{
	// more than 50% digit
		Account account = new Account("name", "username", "", "T@1qwert1234567890");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		System.out.println(errorMessage + "++++++");
		assertTrue(errorMessage.contains("Too many Digits"));
	}
	
	@Test
	public void create_with_repeating_number_more_that_4() throws Exception{

		// with duplicate repeat characters more than 4 digit character
		Account account = new Account("name", "username", "", "@1TheQuick1BronwFox11");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		assertTrue(errorMessage.contains("At least 1 Upper case, 1 lower case ,least 1 numeric, 1 special character"));
	}
	
	@Test
	public void create_with_duplicate_repeat_character_more_than_4_alphabets() throws Exception{
		// with duplicate repeat characters more than 4 alphabet character
		Account account = new Account("name", "username", "", "@1TheQuick1BronwFoxjumpsoverthelazydog");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		assertTrue(errorMessage.contains("Too many Duplicate Character"));
	}
	
	@Test 
	public void create_with_repeating_special_character() throws Exception{
		// with duplicate repeat characters more than 4 Special character
		Account account = new Account("name", "username", "", "@1TheQuick1@Bronw@Fox@");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable()).andReturn().getResolvedException().getMessage();
		System.out.println(errorMessage + "++++++");
		assertTrue(errorMessage.contains("Too many Duplicate Character"));
	}

	@Test
	public void create_account() throws Exception{
		Account account = new Account("Sample Name 1", "sampleName1", "", "@1TheQuick1BronwFox");
		mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test 
	public void update_password() throws Exception{
		Account account = new Account("Sample Name 2", "sampleName2", "", "@1TheQuick1BronwFox");
		mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		account = new Account("Sample Name 2", "sampleName2", "@1TheQuick1BronwFox", "@1Qwertyuioplkjhgfds");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
	
	@Test 
	public void update_password_oldpassword_dont_match() throws Exception{
		Account account = new Account("Sample Name 3", "sampleName3", "", "@1TheQuick1BronwFox1");
		mvc.perform(MockMvcRequestBuilders.post("/account/createAccount").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
		
		account = new Account("Sample Name 3", "sampleName3", "@1TheQuick1BronwFox", "@1Qwertyuioplkjhgfds");
		String errorMessage = mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable())				
				.andReturn().getResolvedException().getMessage();
		assertTrue(errorMessage.contains("Old Password do not match"));
	}
	
	@Test 
	public void update_password_same_as_old() throws Exception{
		Account account = new Account("Sample Name 1", "sampleName1", "@1TheQuick1BronwFox", "@1TheQuick1BronwFox");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	
	@Test 
	public void update_password_80_percent_same_as_old() throws Exception{
		Account account = new Account("Sample Name 1", "sampleName1", "@1TheQuick1BronwFox", "@1TheQuick1Bronwert");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	
	@Test
	public void update_invalid_password_only_small_caps() throws Exception {
		// invalid_password_only_small_caps
		Account account = new Account("SampleName", "SampleUserName", "", "password");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	
	@Test
	public void update_invalid_password_no_special_Character() throws Exception{
		Account account = new Account("name", "username", "", "A1thequickbrownfoxjumpsverthedg");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	
	@Test
	public void update_invalid_password_no_uppercase() throws Exception{
		// invalid password no uppercase
		Account account = new Account("name", "username", "", "1thequickbrownfoxjumpsverthedg");
			mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
					.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isNotAcceptable());
	}
	@Test
	public void update_less_than_18_characters() throws Exception{

		// less than 18 characters
		Account account = new Account("name", "username", "", "1@Pumpsverthedg");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	@Test
	public void update_all_number() throws Exception{
	// all number
		Account account = new Account("name", "username", "", "123456789012345678");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	
	@Test
	public void update_all_special_characters() throws Exception{
		// all special Characters
		Account account = new Account("name", "username", "", "@@@@@@@@@@@@@@@@@@@@@@@@");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	
	@Test 
	public void update_with_invalid_special_character() throws Exception{
		// With invalid Special Character
		Account account = new Account("name", "username", "", "^()&^%$#@!(*&^%$#@!($%$*&^%%$");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	@Test 
	public void update_empty_password() throws Exception{
		// empty password
		Account account = new Account("name", "username", "", "");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	@Test
	public void update_more_than_50_percent_digit() throws Exception{
	// more than 50% digit
		Account account = new Account("name", "username", "", "@1thequickbrow123456789076543");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	
	@Test
	public void update_with_repeating_number_more_that_4() throws Exception{

		// with duplicate repeat characters more than 4 digit character
		Account account = new Account("name", "username", "", "@1TheQuick1BronwFox11");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	
	@Test
	public void update_with_duplicate_repeat_character_more_than_4_alphabets() throws Exception{
		// with duplicate repeat characters more than 4 alphabet character
		Account account = new Account("name", "username", "", "@1TheQuick1BronwFoxjumpsoverthelazydog");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	
	@Test 
	public void update_with_repeating_special_character() throws Exception{
		// with duplicate repeat characters more than 4 Special character
		Account account = new Account("name", "username", "", "@1TheQuick1@Bronw@Fox@");
		mvc.perform(MockMvcRequestBuilders.post("/account/updatePassword").content(asJsonString(account))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotAcceptable());
	}
	private String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}

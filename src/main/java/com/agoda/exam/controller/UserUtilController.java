package com.agoda.exam.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.agoda.exam.bean.Account;
import com.agoda.exam.exception.ResourceNotFoundException;
import com.agoda.exam.service.AccountService;

@RestController
@RequestMapping("account")
public class UserUtilController {
    private static final String template = "Hello World!";
    @Autowired
    AccountService accountService;
	@RequestMapping(value = "/encrypt", method=RequestMethod.GET)
    public String encrypt(@RequestParam(value="password", defaultValue="World")String password)
    		throws InvalidKeyException, NoSuchAlgorithmException, 
    		InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, 
    		UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
		boolean value = accountService.getAccountById((long)1).isPresent();
        return accountService.encryptPassword(password);
    }
	
	@RequestMapping(value = "/decrypt", method=RequestMethod.GET)
    public String decrypt(@RequestParam(value="password", defaultValue="World")String password) throws 
    InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, 
    InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, IOException{
        return accountService.decryptPassword(password);
    }
	
	@RequestMapping(value = "/createAccount", method=RequestMethod.POST, consumes = {"text/plain", "application/json"})
	public Account createAccount(@RequestBody Account account) 
    		throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, 
    		InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
    	return accountService.createAccount(account);
    }
	
	@RequestMapping(value = "/updatePassword", method=RequestMethod.POST, consumes = {"text/plain", "application/json"})
    public Account updatePassword(@RequestBody Account account) 
    		throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, 
    		InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {
    	return accountService.updatePassword(account);
    }
}

package com.agoda.exam.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.Optional;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agoda.exam.bean.Account;
import com.agoda.exam.exception.ResourceNotFoundException;
import com.agoda.exam.repository.AccountRepository;
@Service
public class AccountService {
	@Autowired
	AccountRepository accountRepository;
	@Autowired 
	Util util;
	public String encryptPassword(String password) throws InvalidKeyException, 
		NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, 
		InvalidAlgorithmParameterException, UnsupportedEncodingException, 
		IllegalBlockSizeException, BadPaddingException {
		return util.encrypt("id", password);
	}
	
	public String decryptPassword(String password) throws InvalidKeyException, NoSuchAlgorithmException,
		InvalidKeySpecException, NoSuchPaddingException, InvalidAlgorithmParameterException, 
		UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, IOException {
		return util.decrypt("id", password);
	}
	
	public Optional<Account> getAccountById(Long id) {
		Iterable<Account> test = accountRepository.findAll();
		
		return accountRepository.findById(id);
	}
	
	public Account updatePassword(Account account) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, 
	NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, 
	IllegalBlockSizeException, BadPaddingException, IOException, ResourceNotFoundException{
		Optional<Account> accountOld = accountRepository.findByUsername(account.getUsername());
		Account tempAcc = null;
		if (accountOld.isPresent()) {
			String oldPassword = util.decrypt(accountOld.get().getUsername(), accountOld.get().getPassword());
			if(oldPassword.equals(account.getOldpassword())) {
			if (!oldPassword.equals(account.getPassword())) {
				if (validateNewPassword(account.getPassword())) {
					if(util.similarity(account.getPassword(), oldPassword) < .8) {
						tempAcc = accountOld.get();
						tempAcc.setOldpassword(tempAcc.getPassword());
						tempAcc.setPassword(util.encrypt(tempAcc.getUsername(), account.getPassword()));
						tempAcc.setLastmodifieddate(new Date());
						tempAcc = accountRepository.save(tempAcc);
					}else {
						throw new ResourceNotFoundException("New password is at least 80% similar to the old one");
					}
				}
			}else {
				throw new ResourceNotFoundException("Old and new password are the same");
			}
			}else {
				throw new ResourceNotFoundException("Old Password do not match");
			}
			
		}else {
			throw new ResourceNotFoundException("Account is not found");
		}
		return tempAcc;
	}
	
	public Account createAccount (Account account) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException,
							NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, 
							IllegalBlockSizeException, BadPaddingException, ResourceNotFoundException {
		boolean returnVal = false;
		Account newAccount = null;
		if(validateNewPassword(account.getPassword())) {
			newAccount = new Account(account.getName(), account.getUsername(), null, util.encrypt(account.getUsername(), account.getPassword()));
			newAccount = accountRepository.save(newAccount);
		}
		return newAccount;
	}
	
	
	public boolean validateNewPassword(String password) throws ResourceNotFoundException{
		boolean returnVal = true;
		double numberDigit = (double)password.length() - util.getNumberOccurence(password, "\\D");
		int numberSpecial = util.getNumberOccurence(password, "\\W");
		if(password.length() < util.MAXLENGTH) {
			throw new ResourceNotFoundException("Password is too short");			
		}
		if(!util.isMatchingRegex(password)) {
			throw new ResourceNotFoundException("Invalid Password requirement : At least 1 Upper case, 1 lower case ,least 1 numeric, 1 special character");
		}
		if(util.hasDuplicate(password)) {
			throw new ResourceNotFoundException("Too many Duplicate Character");
		}
		if( (numberDigit/password.length()) >= .5)
			throw new ResourceNotFoundException("Too many Digits");
		if(numberSpecial > util.MAXCOUNT)
			throw new ResourceNotFoundException("Too many Special Character");
		
		return returnVal;
	}
}

package com.agoda.exam.repository;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.agoda.exam.AgodaApplication;
import com.agoda.exam.bean.Account;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AgodaApplication.class)
public class AccountRepositoryTest {

	@Autowired
	AccountRepository accountRepository;

	@Test // check if able to retrieve data.
	public void findAll() {
		List<Account> accounts = (List<Account>) accountRepository.findAll();
		assertFalse(accounts.isEmpty());
	}
	
	@Test // find By Id
	public void byId() {
		// good scenario. 
		Optional<Account> accounts = accountRepository.findById(new Long(1));
		assertTrue(accounts.isPresent());
		// bad Scenario 
		accounts = accountRepository.findById(new Long(12));
		assertFalse(accounts.isPresent());
	}
	
	@Test // find By UserName
	public void byUserName() {
		Optional<Account> accounts = accountRepository.findByUsername("user_name1");
		assertTrue(accounts.isPresent());
		accounts = accountRepository.findByUsername("user_name12");
		assertFalse(accounts.isPresent());
	}

}

package com.agoda.exam.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.agoda.exam.bean.Account;

public interface AccountRepository extends CrudRepository<Account, Long>{
	Optional<Account> findById(Long id);
	Optional<Account> findByUsername(String username);
}
